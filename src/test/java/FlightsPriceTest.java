import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.api.*;

/**
 * Test to verify whether with roundtrip flight search
 * correct results below certain price are returned.
 * In this case simple @ParameterizedTest with @ValueSource
 * is used, as it's convenient with no human involvement.
 * Though if it was required to create input from console
 * parameters it's possible to use @Parameters with appropriate
 * factory class.
 *
 * @author  DM
 * @version 1.0
 * @since   2020-11-12
 */
@DisplayName("Flights below certain price verification")
public class FlightsPriceTest extends BaseTest{

    @DisplayName("Verify that result correspond to price filter")
    @ParameterizedTest
    @CsvSource({"London (LCY),Minsk (MSQ),07.12.2020,19.12.2020,800",
                "Minsk (MSQ),London (LCY),17.12.2020,24.12.2020,300"})
    void check_list_of_flights_with_correct_price_returned(String from_air, String to_air, String from_date,
                                                                                          String to_date, String price){
        HomePage homepage = new HomePage(driver);
        homepage.accept_cookies();
        FlightsPage flightpage = homepage.click_flights_button();
        flightpage.load_result_list_with_no_price_set(from_air, to_air, from_date, to_date);
        Integer requested_price = Integer.parseInt(price);
        flightpage.add_price_filter(requested_price);
        Integer results_max_price = flightpage.get_top_result_price();
        Assertions.assertTrue(results_max_price <= requested_price);
    }

}
