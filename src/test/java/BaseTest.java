import org.openqa.selenium.*;
import org.junit.jupiter.api.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

/**
 * Basic implementation of selenium test class.
 *
 * @author  DM
 * @version 1.0
 * @since   2020-11-12
 */
public class BaseTest {

    protected static WebDriver driver;
    private String baseURL = "https://www.kayak.ch";

    @BeforeAll
    static void initAll() {
    }

    @BeforeEach
    void init() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/geckodriver.exe");
        FirefoxOptions options = new FirefoxOptions()
            .addPreference("browser.cache.disk.enable", false)
            .addPreference("browser.cache.memory.enable", false)
            .addPreference("browser.cache.offline.enable", false)
            .addPreference("browser.cache.disk_cache_ssl", false)
            .addPreference("accessibility.disablecache", true)
            .addPreference("network.http.use-cache", false);
        driver = new FirefoxDriver(options);
        driver.get(baseURL);
        driver.manage().window().maximize();

    }

    @AfterEach
    void tearDown() {
        driver.close();
    }

    @AfterAll
    static void tearDownAll() {
        driver.quit();
    }
}
