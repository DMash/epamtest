import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.apache.commons.text.StringEscapeUtils;
import java.util.concurrent.TimeUnit;

/**
 * Class implements https://www.kayak.com home page
 * with all the required fields and functions.
 *
 * @author  DM
 * @version 1.0
 * @since   2020-11-12
 */
public class HomePage {
    protected static WebDriver driver;
    private String page_title = "Fl\u00fcge, Hotels & Mietwagen finden | KAYAK";
    By flights_button_element = By.xpath("//div[@class='ZRVS']/a[@href='/flights']");
    By accept_cookies_button_element = By.xpath("//button[contains(@id,'accept')]");
    By cookies_viewport_element = By.xpath("//div[contains(@id, 'cookie-consent-dialog')]");

    /**
     * Class constructor.
     */
    public HomePage(WebDriver driver){
        this.driver = driver;
        // Check on the correct page opened
        if (!new WebDriverWait(driver, 10).until(ExpectedConditions.titleContains(StringEscapeUtils.
                                                                                            unescapeJava(page_title)))){
            throw new IllegalStateException("This is not Kayak Home Page," +
                    " current page is: " + driver.getCurrentUrl());
        }
    }

    /**
     * This method is used to accept coolies and wait for the pop-up
     * to close. Had to add waiting as all other options not always return valid result
     * (viewport of pop-up wasn't closed yet). Need to be investigated and refactored.
     */
    public void accept_cookies(){
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable
                                                                               (accept_cookies_button_element)).click();
        try {
            TimeUnit.SECONDS.sleep(2);
            new WebDriverWait(driver, 10).until(ExpectedConditions.invisibilityOf
                    (driver.findElement(cookies_viewport_element)));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is used to move to anothe page Flights.
     *
     * @return  This returns new Flights page object.
     */
    public FlightsPage click_flights_button() {
        WebElement flights_button = driver.findElement(flights_button_element);
        flights_button.click();
        return new FlightsPage(driver);
    }
}
