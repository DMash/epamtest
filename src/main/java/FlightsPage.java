import org.apache.http.client.utils.URIBuilder;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * Class implements https://www.kayak.com/flights page
 * with all the required fields and functions.
 *
 * @author  DM
 * @version 1.0
 * @since   2020-11-12
 */
public class FlightsPage {
    protected static WebDriver driver;
    private String title_text = "Durchsuche Hunderte Flugangebote auf einmal.";
    By title_element = By.xpath("//section[@class='title-section']/div/h2");
    By from_airport_input_element = By.xpath("//input[contains(@id, 'origin-airport')]");
    By plus_button_element = By.xpath("/descendant::div[contains(@class,'plus-button')][1]");
    By clear_from_airports_element = By.xpath("//div[contains(@id, 'origin-airport-smarty-multi-container')]" +
                                                                "/div/div/button[contains(@class,'remove-selection')]");
    By from_airport_container = By.xpath("//div[contains(@id, 'origin-airport-smarty-multi-container')]");
    By to_airport_input_element = By.xpath("//input[contains(@id, 'destination-airport')]");
    By to_airport_container = By.xpath("//div[contains(@id, 'destination-airport-smarty-multi-container')]");
    By to_airport_display = By.xpath("//div[contains(@id, 'destination-airport-display')]");
    By date_from_input_element = By.xpath("//div[contains(@id, 'depart-input')]");
    By date_to_input_element = By.xpath("//div[contains(@id, 'return-input')]");
    By search_button_element = By.xpath("//button[contains(@id,'submit') and contains(@class, 'search')]");
    By list_of_airports = By.xpath("//ul[@class='flight-smarty']/li");
    By date_from_div_element = By.xpath("//div[contains(@id, 'depart')]");
    By date_to_div_element = By.xpath("//div[contains(@id, 'return')]");
    By email_pop_button_element = By.xpath("//input[contains(@id, 'useraddress')]");
    By explore_pane_element = By.xpath("//ul/li[@data-short-name='anywhere']");
    By top_result_price_element = By.xpath("//div[@id='searchResultsList']/div/div[1]//span[@class='price-text']");

    /**
     * Class constructor.
     */
    public FlightsPage(WebDriver driver){
        this.driver = driver;
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(title_element));
       // Check on the correct page opened
        if (!driver.findElement(title_element).getText().equals(title_text)){
            throw new IllegalStateException("This is not Kayak Flights Page," +
                    " current page is: " + driver.getCurrentUrl());
        }
    }

    /**
     * Method implements filling search filters and starting search.
     *
     * Found an issue here when styles are not loaded (+ is not available).
     * Happens sometimes and exception is returned. Might be cache issue.
     * Need to investigate and refactor.
     *
     * @param from_air Value of the airport from.
     * @param to_air Value of the airport to.
     * @param from_date Value of the date from.
     * @param to_date Value of the date to.
     */
    public void load_result_list_with_no_price_set(String from_air, String to_air, String from_date, String to_date){
        WebElement from_airport = driver.findElement(from_airport_input_element);
        //clear fields, set value and select appropriate
        Actions act = new Actions(driver);
        // this is for now the CSS not loaded issue handling
        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.
                    visibilityOfElementLocated(plus_button_element));
        } catch (TimeoutException e){
            System.out.println("CSS is not loaded issue");
            System.out.println("StackTrace: ");
            e.printStackTrace();
            driver.quit();
            System.exit(1);
        }
        act.moveToElement(driver.findElement(plus_button_element)).click().perform();
        new WebDriverWait(driver, 10).until(ExpectedConditions.
                presenceOfElementLocated(clear_from_airports_element));
        driver.findElement(clear_from_airports_element).click();
        from_airport.sendKeys(from_air);
        act.moveToElement(new WebDriverWait(driver, 10).until(ExpectedConditions.
                                                        presenceOfElementLocated(list_of_airports))).click().perform();
        new WebDriverWait(driver, 10).until(ExpectedConditions.
                presenceOfElementLocated(from_airport_container));
        //setting to airport value
        act.moveToElement(driver.findElement(to_airport_display)).click().perform();
        WebElement to_airport = driver.findElement(to_airport_input_element);
        to_airport.sendKeys(to_air);
        new WebDriverWait(driver,10).until(ExpectedConditions.
                                                                    invisibilityOfElementLocated(explore_pane_element));
        act.moveToElement(new WebDriverWait(driver, 10).until(ExpectedConditions.
                                                       presenceOfElementLocated(list_of_airports))).click().perform();
        new WebDriverWait(driver, 10).until(ExpectedConditions.
                presenceOfElementLocated(to_airport_container));
        // setting from date value
        act.moveToElement(driver.findElement(date_from_div_element)).click().perform();
        WebElement date_from = driver.findElement(date_from_input_element);
        act.moveToElement(date_from).sendKeys(from_date).build().perform();
        date_from.sendKeys(Keys.ESCAPE);


        //setting to date value
        act.moveToElement(driver.findElement(date_to_div_element)).click().perform();
        WebElement date_to = driver.findElement(date_to_input_element);
        date_to.sendKeys(to_date);

        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement search_button = driver.findElement(search_button_element);
        act.moveToElement(search_button).click().perform();
        new WebDriverWait(driver, 10).until(ExpectedConditions.
                                                                presenceOfElementLocated(email_pop_button_element));
        act.sendKeys(Keys.ESCAPE).perform();
    }

    /**
    * Method implements adding price to filters and applying sorting,
    * assuming in this case that it works as required.
    * Verification of case when specified price is less than the minimum
    * one for the result list is skipped for now. Should be refactored.
    * Implemented via request url changing (it should be done via UI, but
    * need time ti investigate how to do it, so skipped for now).
    *
    * @param price Value of the max price.
    */
    public void add_price_filter(int price) {
        String url = driver.getCurrentUrl();
        url = url.substring(0,url.indexOf("?"));
        try {
            URIBuilder uri = new URIBuilder(url);
            uri.addParameter("fs","price=-"+price);
            uri.addParameter("sort", "price_b");
            uri.build();
            String result_url = URLDecoder.decode(uri.toString(), "UTF-8");
            driver.get(result_url);
            // Has to use this wait as results are loading dynamically and for now don't know which element
            // might be a flag of processed loading. Need to investigate.
            TimeUnit.SECONDS.sleep(20);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method returns the price of a top result
     *
     * @return price Found top result price.
     */
    public int get_top_result_price(){
        try {
            WebElement price_tag = driver.findElement(top_result_price_element);
            int price = new Scanner(price_tag.getText()).useDelimiter("\\D+").nextInt();
            return price;
        } catch (NoSuchElementException e) {
            System.out.println("No results found. Change filter parameters.");
            return 0;
        }

    }

}
