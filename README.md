# Project Title

EPAM Kayak testing task.

## Requirements

Create Web UI test which proves that https://www.kayak.ch/ always return a roundtrip flight below certain price. The test should take following parameters:

* From and To airports
* Date range
* Max price

### Notes

There are several things worth mentioning:

* from and to airports should have logical names, f.e. Minsk(MSQ)
* dates should be within curent + 1 month (otherwise test logic will be much complicated)
* valid price should be used (f.e. 1 is smth that this website cannot take in it's slider)
* there is an issue with not loaded CSS, which is sometimes encountered
* there are ninja pop-ups which somehow are shown in random moments and may spoil the work of a test
* no specific reporting was implemented (though it's easy to add one if required) 
* please take a look into comments througout the code 
